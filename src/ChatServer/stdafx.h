// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"
#include <stdio.h>
#include <tchar.h>
#include <iostream>

//thrift
#include <thrift/server/TThreadedServer.h>
#include <thrift/protocol/TJSONProtocol.h>
#include <thrift/transport/TPipe.h>
#include <thrift/transport/TPipeServer.h>
#include <thrift/TApplicationException.h>
#include "gen-cpp/ChatService.h"