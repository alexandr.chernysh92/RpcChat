#include "stdafx.h"
#include "Server.h"

int _tmain(int, _TCHAR*[])
{
    try
    {
        chat::Server server;
        std::cout << "Server started\n";
        server.Serve();

        std::cout << "Done";
    }
    catch (const std::exception& ex)
    {
        std::cout << "[error] " << ex.what();
    }

    return 0;
}
