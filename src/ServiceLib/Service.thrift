﻿namespace cpp chatgen

exception ConnectionError
{
    1: string errorMessage
}
exception InternalError
{
    1: string errorMessage
}

service ClientService
{
    oneway void IncomingMessage(1:string message, 2:string senderName, 3:bool isBroadcast)
    oneway void UserConnected(1:string userName)
    oneway void UserDisconnected(1:string userName)
}

service ChatService
{
    void Connect(1:string userName) throws (1:ConnectionError ce, 2:InternalError ie)

    oneway void Ping();
    oneway void Disconnect()
    set<string> GetUserList()

    oneway void SendDirectMessage(1:string message, 2:string toUserName)
    oneway void SendBroadcastMessage(1:string message)
}

