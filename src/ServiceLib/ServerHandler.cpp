#include "stdafx.h"
#include "ServerHandler.h"
#include "Utils.h"

using namespace chat;
using namespace chatgen;

ServerHandler::ServerHandler(UserPtr currentUser, Users& users)
    : currentUser_(currentUser)
    , users_(users)
{}

void ServerHandler::Connect(const std::string& userName)
{
    if (users_.find(userName) != users_.end())
    {
        utils::Throw<ConnectionError>("The user already exists");
    }

    userName_ = userName;
    CheckCurrecntUser();

    for (auto& user : users_)
    {
        user.second->OnUserConnected(userName);
    }

    users_.emplace(userName, currentUser_);
    std::cout << "[" << userName_ << "][connected]\n";
}

void ServerHandler::Ping()
{
    std::cout << "[" << userName_ << "][ping]\n";
}

void ServerHandler::Disconnect()
{
    CheckCurrecntUser();

    users_.erase(userName_);
    
    for (auto& user : users_)
    {
        user.second->OnUserDisconnected(userName_);
    }

    std::cout << "[" << userName_ << "][disconnected]\n";
    userName_ = "";
    currentUser_->Disconnect();
}

void ServerHandler::GetUserList(std::set<std::string>& usersList)
{
    CheckCurrecntUser();
    for (const auto& userName : users_)
    {
        usersList.insert(userName.first);
    }

    std::cout << "[" << userName_ << "][get user list]\n";
}

void ServerHandler::SendDirectMessage(const std::string& message, const std::string& toUserName)
{
    CheckCurrecntUser();
    users_[toUserName]->SendDirectMessage(message, userName_, false);

    std::cout << "[" << userName_ << "][sent message][to " + toUserName + "]\n";
}

void ServerHandler::SendBroadcastMessage(const std::string& message)
{
    CheckCurrecntUser();
    for (const auto& user : users_)
    {
        user.second->SendDirectMessage(message, userName_, true);
    }

    std::cout << "[" << userName_ << "][sent broadcast message]\n";
}

void ServerHandler::CheckCurrecntUser()
{
    if (userName_.empty())
    {
        utils::Throw<ConnectionError>("Client isn't connected");
    }
}