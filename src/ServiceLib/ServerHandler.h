#pragma once
#include "Types.h"
#include "User.h"

namespace chat
{
    class ServerHandler : public chatgen::ChatServiceIf
    {
    public:
        explicit ServerHandler(UserPtr currentUser, Users& users);

        void Connect(const std::string& userName) override;

        void Ping();
        void Disconnect() override;
        void GetUserList(std::set<std::string>& usersList) override;

        void SendDirectMessage(const std::string& message, const std::string& toUserName) override;
        void SendBroadcastMessage(const std::string& message) override;
    private:
        void CheckCurrecntUser();

    private:
        std::string userName_;
        UserPtr     currentUser_;
        Users&      users_;
    };
}

