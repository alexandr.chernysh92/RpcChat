#include "stdafx.h"
#include "ClientInstanceFactory.h"
#include "ServerHandler.h"

using namespace chat;
using namespace chatgen;

ClientInstanceFactory::ClientInstanceFactory(ClientsPtr clients)
    : clients_(clients)
{}

TProcessorPtr ClientInstanceFactory::getProcessor(const TConnectionInfo& connection)
{
    auto chatClientPtr = boost::make_shared<ClientServiceClient>(
        boost::make_shared<TBinaryProtocol>(connection.transport)
        );

    auto userPtr = boost::make_shared<User>(
        chatClientPtr,
        connection.transport
        );

    auto handler = boost::make_shared<ServerHandler>(
        userPtr,
        users_
        );

    return boost::make_shared<ChatServiceProcessor>(handler);
}

