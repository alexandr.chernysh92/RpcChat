#pragma once
#include "stdafx.h"

namespace chat
{
    namespace utils
    {
        template <typename T>
        void Throw(const std::string& message)
        {
            T ex;
            ex.errorMessage = message;
            throw ex;
        }
    }
}