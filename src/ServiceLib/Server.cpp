#include "stdafx.h"
#include "Server.h"
#include "ClientInstanceFactory.h"
#include "Settings.h"

using namespace chat;
using namespace chatgen;
using namespace apache::thrift::server;
using namespace apache::thrift::transport;

Server::Server() 
    : clients_(boost::make_shared<Clients>())
{
    auto serverTransport    = boost::make_shared<TServerSocket>(settings::SERVER_PORT);
    auto transportFactory   = boost::make_shared<TBufferedTransportFactory>();
    auto protocolFactory    = boost::make_shared<TBinaryProtocolFactory>();
    auto processorFactory   = boost::make_shared<ClientInstanceFactory>(clients_);

    server_ = boost::make_shared<TThreadedServer>(
        processorFactory,
        serverTransport,
        transportFactory,
        protocolFactory);
}

void Server::Serve()
{
    server_->serve();
}
