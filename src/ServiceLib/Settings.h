namespace chat
{
    namespace settings
    {
        const static size_t         SERVER_PORT     = 9090;
        const static std::string    SERVER_HOST     = "localhost";
    }
}