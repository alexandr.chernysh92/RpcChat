/**
 * Autogenerated by Thrift Compiler (0.10.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef Service_TYPES_H
#define Service_TYPES_H

#include <iosfwd>

#include <thrift/Thrift.h>
#include <thrift/TApplicationException.h>
#include <thrift/TBase.h>
#include <thrift/protocol/TProtocol.h>
#include <thrift/transport/TTransport.h>

#include <thrift/cxxfunctional.h>


namespace chatgen {

class ConnectionError;

class InternalError;

typedef struct _ConnectionError__isset {
  _ConnectionError__isset() : errorMessage(false) {}
  bool errorMessage :1;
} _ConnectionError__isset;

class ConnectionError : public ::apache::thrift::TException {
 public:

  ConnectionError(const ConnectionError&);
  ConnectionError& operator=(const ConnectionError&);
  ConnectionError() : errorMessage() {
  }

  virtual ~ConnectionError() throw();
  std::string errorMessage;

  _ConnectionError__isset __isset;

  void __set_errorMessage(const std::string& val);

  bool operator == (const ConnectionError & rhs) const
  {
    if (!(errorMessage == rhs.errorMessage))
      return false;
    return true;
  }
  bool operator != (const ConnectionError &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ConnectionError & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

  virtual void printTo(std::ostream& out) const;
  mutable std::string thriftTExceptionMessageHolder_;
  const char* what() const throw();
};

void swap(ConnectionError &a, ConnectionError &b);

inline std::ostream& operator<<(std::ostream& out, const ConnectionError& obj)
{
  obj.printTo(out);
  return out;
}

typedef struct _InternalError__isset {
  _InternalError__isset() : errorMessage(false) {}
  bool errorMessage :1;
} _InternalError__isset;

class InternalError : public ::apache::thrift::TException {
 public:

  InternalError(const InternalError&);
  InternalError& operator=(const InternalError&);
  InternalError() : errorMessage() {
  }

  virtual ~InternalError() throw();
  std::string errorMessage;

  _InternalError__isset __isset;

  void __set_errorMessage(const std::string& val);

  bool operator == (const InternalError & rhs) const
  {
    if (!(errorMessage == rhs.errorMessage))
      return false;
    return true;
  }
  bool operator != (const InternalError &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const InternalError & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

  virtual void printTo(std::ostream& out) const;
  mutable std::string thriftTExceptionMessageHolder_;
  const char* what() const throw();
};

void swap(InternalError &a, InternalError &b);

inline std::ostream& operator<<(std::ostream& out, const InternalError& obj)
{
  obj.printTo(out);
  return out;
}

} // namespace

#endif
