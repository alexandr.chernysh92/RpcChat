#pragma once
#include "User.h"

namespace chat
{
    class ClientInstanceFactory : public TProcessorFactory
    {
    public:
        explicit ClientInstanceFactory(ClientsPtr clients);
        TProcessorPtr getProcessor(const TConnectionInfo& connection) override;

    private:
        ClientsPtr      clients_;
        TProcessorPtr   processor_;
        Users           users_;
    };
}