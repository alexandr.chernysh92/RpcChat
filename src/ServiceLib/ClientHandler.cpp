#include "stdafx.h"
#include "ClientHandler.h"

using namespace chat;

ClientHandler::ClientHandler(ChatServiceClientPtr client, std::set<std::string>& users)
    : users_(users)
    , client_(client)
{}

void ClientHandler::IncomingMessage(const std::string& message, const std::string& senderName, const bool isBroadcast)
{
    std::cout << "[" << senderName << "]";
    if (isBroadcast)
    {
        std::cout << "[broadcast]";
    }

    std::cout << message << "\n";
}

void ClientHandler::UserConnected(const std::string& userName)
{
    std::cout << "[user connected]: " << userName << "\n";
    users_.insert(userName);
}

void ClientHandler::UserDisconnected(const std::string& userName)
{
    std::cout << "[user disconnected]: " << userName << "\n";
    users_.erase(userName);
}