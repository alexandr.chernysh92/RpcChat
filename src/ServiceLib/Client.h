#pragma once
#include "Types.h"

namespace chat
{
    class Client final
    {
    public:
        Client();
        ~Client();

        Client(Client&) = delete;

        void Connect(const std::string& userName);
        void Ping();
        void Disconnect();

        const std::set<std::string>& GetConnectedUsers() const;

        void SendMessageToUser(const std::string& message, const std::string& userName);
        void SendBroadcastMessage(const std::string& message);

    private:
        ChatServiceClientPtr    client_;
        TProtocolPtr            protocol_;
        TTransportPtr           transport_;

        std::set<std::string>   users_;
        std::thread             processorThread_;
    };
}