#pragma once
#include "Types.h"

namespace chat
{
    class User
    {
    public:
        User(ClientServicePtr client, TTransportPtr transport);

        void Disconnect();
        void SendDirectMessage(const std::string& message, const std::string& sender, bool isBroadcast);
        void OnUserConnected(const std::string& userName);
        void OnUserDisconnected(const std::string& userName);

    private:
        ClientServicePtr    client_;
        TTransportPtr       transport_;
    };

    using UserPtr   = boost::shared_ptr<User>;
    using Users     = std::map<std::string, UserPtr>;
}

