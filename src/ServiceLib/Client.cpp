#include "stdafx.h"
#include "Client.h"
#include "Settings.h"
#include "ClientHandler.h"

using namespace chat;
using namespace chatgen;

Client::Client()
{
    auto socket = boost::make_shared<TSocket>(settings::SERVER_HOST, settings::SERVER_PORT);

    transport_  = boost::make_shared<TBufferedTransport>(socket);
    protocol_   = boost::make_shared<TBinaryProtocol>(transport_);
    client_     = boost::make_shared<ChatServiceClient>(protocol_);
}

Client::~Client()
{
    Disconnect();
}

void Client::Connect(const std::string& userName)
{
    transport_->open();
    int i = 0;
    client_->Connect(userName);
    client_->GetUserList(users_);

    auto serverMessageProcessor = [&]()
    {
        auto handler = boost::make_shared<ClientHandler>(client_, users_);
        auto processor = boost::make_shared<ClientServiceProcessor>(handler);

        try
        {
            while (transport_->isOpen())
            {
                processor->process(protocol_, protocol_, NULL);
            }
        }
        catch (const std::exception& ex)
        {
            std::cout << "[Disconnected]" << ex.what() << "\n";
        }
    };

    processorThread_ = std::thread(serverMessageProcessor);
}

void Client::Ping()
{
    client_->Ping();
}

void Client::Disconnect()
{
    if (transport_->isOpen())
    {
        client_->Disconnect();
        transport_->close();

        if (processorThread_.joinable())
        {
            processorThread_.join();
        }
    }
}

const std::set<std::string>& Client::GetConnectedUsers() const
{
    return users_;
}

void Client::SendMessageToUser(const std::string& message, const std::string& userName)
{
    client_->SendDirectMessage(message, userName);
}

void Client::SendBroadcastMessage(const std::string& message)
{
    client_->SendBroadcastMessage(message);
}