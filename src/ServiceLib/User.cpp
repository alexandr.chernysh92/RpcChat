#include "stdafx.h"
#include "User.h"

using namespace chat;

User::User(ClientServicePtr client, TTransportPtr transport)
    : client_(client)
    , transport_(transport)
{}

void User::Disconnect()
{
    transport_->close();
}

void User::SendDirectMessage(const std::string& message, const std::string& sender, bool isBroadcast)
{
    client_->IncomingMessage(message, sender, isBroadcast);
}

void User::OnUserConnected(const std::string& userName)
{
    client_->UserConnected(userName);
}

void User::OnUserDisconnected(const std::string& userName)
{
    client_->UserDisconnected(userName);
}