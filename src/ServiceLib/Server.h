#pragma once
#include "Types.h"

namespace chat
{
    class Server final
    {
    public:
        Server();
        void Serve();

    private:
        TThreadedServerPtr  server_;
        ClientsPtr          clients_;
    };
}
