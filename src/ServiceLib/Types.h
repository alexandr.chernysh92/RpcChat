#pragma once

namespace chatgen
{
    class ChatServiceClient;
    class ClientServiceClient;
}

namespace apache
{
    namespace thrift
    {
        class TProcessor;
        class TProcessorFactory;
        struct TConnectionInfo;

        namespace protocol
        {
            class TProtocol;
        }

        namespace transport
        {
            class TTransport;
            class TSocket;
            class TBufferedTransport;
        }

        namespace server
        {
            class TThreadedServer;
            class TSimpleServer;
        }
    }
}

namespace chat
{
    using TProtocolPtr          = boost::shared_ptr<apache::thrift::protocol::TProtocol>;
    using TTransportPtr         = boost::shared_ptr<apache::thrift::transport::TTransport>;
    using TProcessorPtr         = boost::shared_ptr<apache::thrift::TProcessor>;
    using TThreadedServerPtr    = boost::shared_ptr<apache::thrift::server::TThreadedServer>;
    using TSimpleServerPtr      = boost::shared_ptr<apache::thrift::server::TSimpleServer>;
    using ChatServiceClientPtr  = boost::shared_ptr<chatgen::ChatServiceClient>;
    using ClientServicePtr      = boost::shared_ptr<chatgen::ClientServiceClient>;

    using TProcessorFactory     = apache::thrift::TProcessorFactory;
    using TConnectionInfo       = apache::thrift::TConnectionInfo;
    using TBinaryProtocol       = apache::thrift::protocol::TBinaryProtocol;
    using TSocket               = apache::thrift::transport::TSocket;
    using TBufferedTransport    = apache::thrift::transport::TBufferedTransport;
    
    using Clients               = std::list<ChatServiceClientPtr>;
    using ClientsPtr            = boost::shared_ptr<Clients>;
}