#pragma once
#include "Types.h"

namespace chat
{
    class ClientHandler : public chatgen::ClientServiceIf
    {
    public:
        ClientHandler(ChatServiceClientPtr client, std::set<std::string>& users);

        void IncomingMessage(const std::string& message, const std::string& senderName, const bool isBroadcast) override;
        void UserConnected(const std::string& userName) override;
        void UserDisconnected(const std::string& userName) override;

    private:
        std::set<std::string>&  users_;
        ChatServiceClientPtr    client_;
    };
}
