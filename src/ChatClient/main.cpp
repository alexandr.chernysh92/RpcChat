#include "stdafx.h"
#include "Client.h"

std::unique_ptr<chat::Client> g_client;

int ReceiveCommand();
void ShowHelp();
void Command_ShowAllUsers();
void Command_Connect();
void Command_Disconnect();
void Command_Chat();
void Command_Ping();
void RunCommnad(int command);

void ShowError(const std::string& errorMessage);
std::string ReadLine();
bool CheckClient();

void ShowHelp()
{
    std::cout << "-------------------------------\n";
    std::cout << "0. exit\n";
    std::cout << "1. connect\n";
    std::cout << "2. disconect\n";
    std::cout << "3. show all users\n";
    std::cout << "4. send Message\n";
    std::cout << "5. ping server\n";
}

int ReceiveCommand()
{
    ShowHelp();

    int command = -1;
    std::cin >> command;
    return command;
}

void Command_ShowAllUsers()
{
    if (CheckClient())
    {
        const std::set<std::string>& users = g_client->GetConnectedUsers();
        std::cout << "Users: " << users.size() << "\n";

        size_t i = 1;
        for (const auto& user : users)
        {
            std::cout << i << ". " << user << "\n";
            ++i;
        }
    }
}

void Command_Connect()
{
    std::cout << "Your name:";
    std::string userName = ReadLine();
    
    try
    {
        g_client = std::make_unique<chat::Client>();
        g_client->Connect(userName);

        std::cout << "Connected\n";
    }
    catch (const chatgen::ConnectionError& ex)
    {
        ShowError("Cannot connect to server: " + ex.errorMessage);
    }
}

void Command_Disconnect()
{
    if (CheckClient())
    {
        g_client->Disconnect();
        std::cout << "Disconnected\n";
    }    
}

void Command_Chat()
{
    if (!CheckClient())
    {
        std::cout << "Chat closed\n";
        return;
    }

    std::cout << ">Enter user name or 0 to broadcast\n";
    std::string sender = ReadLine();

    std::cout << ">Write message\n";
    std::string message = ReadLine();

    if (sender == "0")
    {
        g_client->SendBroadcastMessage(message);
    }
    else
    {
        g_client->SendMessageToUser(message, sender);
    }


    std::cout << ">Sent. Enter to continue\n";
    std::getchar();
}

void Command_Ping()
{
    g_client->Ping();
}

void RunCommnad(int command)
{
    try
    {
        switch (command)
        {
        case 0: exit(0);                break;
        case 1: Command_Connect();      break;
        case 2: Command_Disconnect();   break;
        case 3: Command_ShowAllUsers(); break;
        case 4: Command_Chat();         break;
        case 5: Command_Ping();         break;

        default:
            ShowError("Invalid command");
            break;
        }
    }
    catch (const std::exception& ex)
    {
        ShowError(ex.what());
    }
}

void ShowError(const std::string& errorMessage)
{
    std::cout << "\n[error] " << errorMessage.c_str() << "\n";
}

std::string ReadLine()
{
    //todo: WTF??? Do reafctoring!
    std::vector<char> input(256, 0);;
    std::cin.getline(&input.at(0), input.size());

    if (input.at(0) == 0)
    {
        std::string line;
        std::getline(std::cin, line);
        return line;
    }
    else
    {
        return std::string(input.begin(), input.end());
    }
}

bool CheckClient()
{
    if (g_client)
    {
        return true;
    }
    else
    {
        ShowError("Not connected");
        return false;
    }
}

int _tmain(int, _TCHAR*[])
{
    try
    {
        while (true)
        {
            int command = ReceiveCommand();
            RunCommnad(command);
        }
    }
    catch (const std::exception& ex)
    {
        std::cout << "[fatal error]: " << ex.what() << "\n";
    }

	return 0;
}

